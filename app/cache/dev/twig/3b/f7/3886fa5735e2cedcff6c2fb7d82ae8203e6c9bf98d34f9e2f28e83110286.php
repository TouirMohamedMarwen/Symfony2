<?php

/* FrontOfficeBundle::layout.html.twig */
class __TwigTemplate_3bf73886fa5735e2cedcff6c2fb7d82ae8203e6c9bf98d34f9e2f28e83110286 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<title>Wing the Air</title>
<meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />
<link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>
<link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>
<!--[if lte IE 6]><style type=\"text/css\" media=\"screen\">.tabbed { height:420px; }</style><![endif]-->
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/js/jquery.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/js/bootstrap.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"https://apis.google.com/js/platform.js\" async defer></script>

<script>
// Put jQuery  into noConflict mode.
var \$jq = jQuery.noConflict(true);
</script>
<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/js/jquery-1.4.1.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

<script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/js/jquery.jcarousel.pack.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

<script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/js/jquery.slide.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/js/jquery-func.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

</head>
<body>
    
    <div id=\"fb-root\"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = \"//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.6\";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    
<!-- Top -->
<div id=\"top\">
  <div class=\"shell\">
    <!-- Header -->
    <div id=\"header\">
      <h1 id=\"logo\"><a href=\"#\">Urgan Gear</a></h1>
      <div id=\"navigation\">
        <ul>
          <li><a href=\"#\">Home</a></li>
          <li><a href=\"#\">Support</a></li>
          <li><a href=\"#\">My Account</a></li>
          <li><a href=\"#\">The Store</a></li>
          <li class=\"last\"><a href=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\">Logout</a></li>
        </ul>
      </div>
    </div>
    <!-- End Header -->
    <!-- Slider -->
    <div id=\"slider\">
      <div id=\"slider-holder\">
        <ul>
          <li><a href=\"#\"><img src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/css/images/slide1.jpg"), "html", null, true);
        echo "\" alt=\"\" /></a></li>
          <li><a href=\"#\"><img src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/css/images/slide2.jpg"), "html", null, true);
        echo "\" alt=\"\" /></a></li>
          <li><a href=\"#\"><img src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/css/images/slide1.jpg"), "html", null, true);
        echo "\" alt=\"\" /></a></li>
          <li><a href=\"#\"><img src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/css/images/slide2.jpg"), "html", null, true);
        echo "\" alt=\"\" /></a></li>
          <li><a href=\"#\"><img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/css/images/slide1.jpg"), "html", null, true);
        echo "\" alt=\"\" /></a></li>
          <li><a href=\"#\"><img src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/frontoffice/css/images/slide2.jpg"), "html", null, true);
        echo "\" alt=\"\" /></a></li>
        </ul>
      </div>
      <div id=\"slider-nav\"> <a href=\"#\" class=\"prev\">Previous</a> <a href=\"#\"  class=\"next\">Next</a> </div>
    </div>
    <!-- End Slider -->
  </div>
</div>
<!-- Top -->
<!-- Main -->
<div id=\"main\">
  <div class=\"shell\">
    <!-- Search, etc -->
    <div class=\"options\">
      
      <span class=\"left\"><a href=\"#\">Advanced Search</a></span>
      <div class=\"right\"> <span class=\"cart\"> <a  onclick=\"getPanier();\" data-toggle=\"modal\" data-target=\"#myModal\" class=\"cart-ico\">&nbsp;</a> <strong id=\"panier\">\$0.00</strong> </span> <span class=\"left more-links\"> <a href=\"#\">Checkout</a> <a href=\"#\">Details</a> </span> </div>
    </div>
    <!-- End Search, etc -->
    <!-- Content -->
    <div id=\"content\">
        ";
        // line 85
        $this->displayBlock('body', $context, $blocks);
        // line 86
        echo "
    </div>
    <!-- End Content -->
    <!-- Modal -->
<div id=\"myModal\" class=\"modal fade\" role=\"dialog\">
  <div class=\"modal-dialog\">

    <!-- Modal content-->
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
        <h4 class=\"modal-title\">Panier</h4>
      </div>
      <div id=\"listPanier\" class=\"modal-body list-group\">
        
      </div>
      <div class=\"modal-footer\">
          <button type=\"button\" onclick=\"viderPanier();\" class=\"btn btn-default\" data-dismiss=\"modal\" >Vider Panier</button>
          <button type=\"button\" onclick=\"CommandePanier();\" class=\"btn btn-default\" >Commande</button>
      </div>
    </div>

  </div>
</div>
  </div>
    
</div>
<!-- End Main -->

<script>
    syncPanier();
    function CommandePanier(){
        var data=JSON.parse(sessionStorage.panier);
        console.log(data);
        if (data.length>0) {
            location.href=\"";
        // line 121
        echo $this->env->getExtension('routing')->getPath("user_commande");
        echo "\";
            
         
    
        }else{
            //no commande
        }
        
        
    }
    function getPanier(){
        
        var  products=JSON.parse(sessionStorage.panier);
        var html=\"\";
        var somme=0;
        for (var i = 0; i < products.length; i++) {
            somme+=products[i].price;
    html+='<a  class=\"list-group-item\">'+products[i].libel+' :--> \$'+products[i].price+'</a>';
}
     html+='<hr>' ;  
        html+='<a  class=\"list-group-item\">Somme : \$'+somme+'</a>';
        
      \$(\"#listPanier\")[0].innerHTML=html;
    }
    function viderPanier(){
        sessionStorage.panier=JSON.stringify([]);
        syncPanier();
    }
    function syncPanier(){
        
         var p=JSON.parse(sessionStorage.panier);
            var somme=0;
             for(var i=0;i<p.length;i++){
                somme+=p[i].price;
             } 
             \$(\"#panier\")[0].innerHTML=\"\$\"+somme;
    }
</script>
";
        // line 159
        $this->displayBlock('javascripts', $context, $blocks);
        // line 160
        echo "</body>
</html>

";
    }

    // line 85
    public function block_body($context, array $blocks = array())
    {
    }

    // line 159
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "FrontOfficeBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  246 => 159,  241 => 85,  234 => 160,  232 => 159,  191 => 121,  154 => 86,  152 => 85,  128 => 64,  124 => 63,  120 => 62,  116 => 61,  112 => 60,  108 => 59,  96 => 50,  66 => 23,  61 => 21,  56 => 19,  51 => 17,  41 => 10,  37 => 9,  32 => 7,  28 => 6,  21 => 1,);
    }
}
