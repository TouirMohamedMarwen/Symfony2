<?php

/* FrontOfficeBundle:Default:index.html.twig */
class __TwigTemplate_5f4902d50c27631a2a25deb00ea90ee7f4eda4849037fe2ea086afcb1f6a0301 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FrontOfficeBundle::layout.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontOfficeBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "<!-- Tabs -->
      <div class=\"tabs\">
        <ul>
            ";
        // line 10
        $context["i"] = 0;
        // line 11
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 12
            echo "                ";
            if (((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) == 0)) {
                // line 13
                echo "          <li><a href=\"#\" class=\"active\"><span>";
                echo twig_escape_filter($this->env, $context["c"], "html", null, true);
                echo "</span></a></li>
              ";
            } else {
                // line 15
                echo "          <li><a href=\"#\"><span>";
                echo twig_escape_filter($this->env, $context["c"], "html", null, true);
                echo "</span></a></li>
                ";
            }
            // line 17
            echo "          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "        </ul>
      </div>
      <!-- Tabs -->
      <!-- Container -->
      <div id=\"container\">
            
        <div class=\"tabbed\">
         ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 26
            echo "          <div class=\"tab-content\" style=\"display:block;\">
            <div class=\"items\">
              <div class=\"cl\">&nbsp;</div>
              <ul>
                  ";
            // line 30
            $context["i"] = 0;
            // line 31
            echo "                  ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["produits"]) ? $context["produits"] : $this->getContext($context, "produits")));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 32
                echo "                      ";
                if (($this->getAttribute($context["p"], "categorie", array()) == $context["c"])) {
                    // line 33
                    echo "                          ";
                    $context["i"] = ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1);
                    // line 34
                    echo "                <li>
                  <div class=\"image\"> <a href=\"#\"><img src=\"";
                    // line 35
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("bundles/frontoffice/css/images/image" . (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i"))) . ".jpg")), "html", null, true);
                    echo "\" alt=\"\" /></a> </div>
                  <p> Item Number: <span>";
                    // line 36
                    echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "reference", array()), "html", null, true);
                    echo "</span><br />
                      Description : <span >";
                    // line 37
                    echo twig_escape_filter($this->env, twig_truncate_filter($this->env, $this->getAttribute($context["p"], "description", array()), 10, false), "html", null, true);
                    echo "</span><br />
                    Brand Name: <a href=\"#\">";
                    // line 38
                    echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "libelle", array()), "html", null, true);
                    echo "</a> </p>
                  <p class=\"price\">Price: <strong>";
                    // line 39
                    echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "prix", array()), "html", null, true);
                    echo " USD</strong></p>
                  <button id=\"";
                    // line 40
                    echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "idproduit", array()), "html", null, true);
                    echo "\" class=\"btn-sm btn-success\" onclick=\"addToPanier('";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "libelle", array()), "html", null, true);
                    echo "',";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "idproduit", array()), "html", null, true);
                    echo ",";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "prix", array()), "html", null, true);
                    echo ");\" style=\"float: right;\">+</button>
                </li>
                     ";
                    // line 42
                    if (((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) == 4)) {
                        // line 43
                        echo "                         ";
                        $context["i"] = 0;
                        // line 44
                        echo "                    ";
                    }
                    // line 45
                    echo "                ";
                }
                // line 46
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "              </ul>
              <div class=\"cl\">&nbsp;</div>
            </div>
          
          <div class=\"fb-comments\" data-href=\"https://www.facebook.com/tnmallespritcmd/photos/a.1216375445041670.1073741826.1216370048375543/1216375301708351/?type=3&amp;theater\" data-numposts=\"5\"></div>
         <div class=\"fb-share-button\" data-href=\"https://www.facebook.com/tnmallespritcmd/photos/a.1216375445041670.1073741826.1216370048375543/1216375301708351/?type=3&amp;theater\" data-layout=\"button_count\" data-mobile-iframe=\"true\"></div>
          <g:plus action=\"share\"></g:plus>

          </div>
         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "         
        </div>
        <!-- Brands -->
        <div class=\"brands\">
          <h3>Brands</h3>
      </div>
        <!-- End Brands -->
        <!-- Footer -->
        <div id=\"footer\">
          <div class=\"left\"> <a href=\"#\">Home</a> <span>|</span> <a href=\"#\">Support</a> <span>|</span> <a href=\"#\">My Account</a> <span>|</span> <a href=\"#\">The Store</a> <span>|</span> <a href=\"#\">Contact</a> </div>
          <div class=\"right\"> &copy; Sitename.com. Design by <a href=\"http://chocotemplates.com\">ChocoTemplates.com</a> </div>
        </div>
        <!-- End Footer -->
      </div>
      <!-- End Container -->
 ";
    }

    // line 73
    public function block_javascripts($context, array $blocks = array())
    {
        // line 74
        echo "     <script>
         
            if (!sessionStorage.panier) {
    var p=[];
    sessionStorage.panier=JSON.stringify(p);
         }else{
             syncPanier();
         }
         
         
         function addToPanier(libel,id,price){
             var product={
                 libel:libel,
                 id:id,
                 price:price
             };
             var panier=JSON.parse(sessionStorage.panier);
             panier.push(product);
             sessionStorage.panier=JSON.stringify(panier);
             syncPanier();
  
         }
         
     </script>
 
 
 
 ";
    }

    public function getTemplateName()
    {
        return "FrontOfficeBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 74,  182 => 73,  163 => 57,  148 => 47,  142 => 46,  139 => 45,  136 => 44,  133 => 43,  131 => 42,  120 => 40,  116 => 39,  112 => 38,  108 => 37,  104 => 36,  100 => 35,  97 => 34,  94 => 33,  91 => 32,  86 => 31,  84 => 30,  78 => 26,  74 => 25,  65 => 18,  59 => 17,  53 => 15,  47 => 13,  44 => 12,  39 => 11,  37 => 10,  32 => 7,  29 => 3,);
    }
}
