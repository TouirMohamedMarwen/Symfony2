<?php

/* UserBundle:Default:commande.html.twig */
class __TwigTemplate_29205afba93b1c9d76c79236adaf99de1d9c2de9e2ebaff5de8ecb513ed59d70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FrontOfficeBundle::layout.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontOfficeBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "<div id=\"container\">
    <div  id=\"form-cmd\" class=\"form-horizontal\" >

  
 
  
  </div>
</div >
</div >
    
    
";
    }

    // line 15
    public function block_javascripts($context, array $blocks = array())
    {
        // line 16
        echo "    <script>
        
        
        
 var products=JSON.parse(sessionStorage.panier),html=\"\";
 initForm();
 function initForm(){
    for (var i = 0, max = products.length; i < max; i++) {
 html+= '<div class=\"form-group\" id=\"'+products[i].id+'\"><label class=\"control-label col-sm-2\" for=\"prod_'+products[i].id+'\"><button  onclick=\"Supprimer('+products[i].id+')\" class=\"btn btn-danger\">-</button></label><div class=\"col-sm-10\">'+ 
      '<input type=\"text\" disabled class=\"form-control\" id=\"prod_'+products[i].id+'\" placeholder=\"'+products[i].libel+'    : \$'+products[i].price+'\"></div></div>';
    } 
    html+= '<div class=\"form-group\"><div class=\"col-sm-offset-2 col-sm-10\"><button type=\"submit\" onclick=\"Validate()\" class=\"btn btn-default\">Valider</button><button  onclick=\"Ajouter()\" class=\"btn btn-success\">+</button></div>'
    \$(\"#form-cmd\")[0].innerHTML=html;
 }
   function Supprimer(id){
     \$(\"#\"+id).slideUp();
     var index=-1;
     for (var i = 0, max = products.length; i < max; i++) {
    if (products[i].id===id) {
    index=i;
    break;
     }
     }
    
     products.splice(index,1);
     sessionStorage.panier=JSON.stringify(products);
  }
    
    
    
    
    
    
  function Validate(){
       var data = {request : products};

        \$.ajax({
            type: \"POST\",
            url: \"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("user_commande");
        echo "\",
            data: data,
            success: function(data, dataType)
            {
                console.log(data);
                alert(\"commande ajouter pour '\"+data.name+\"' !!\");
                location.href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("front_office_homepage");
        echo "\";
            },

            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error : ' + errorThrown);
            }
        });
  }
    function Ajouter(){
      location.href=\"";
        // line 70
        echo $this->env->getExtension('routing')->getPath("front_office_homepage");
        echo "\";
  }
    </script>
    
    
    
    
";
    }

    public function getTemplateName()
    {
        return "UserBundle:Default:commande.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 70,  99 => 60,  90 => 54,  50 => 16,  47 => 15,  32 => 3,  29 => 2,);
    }
}
