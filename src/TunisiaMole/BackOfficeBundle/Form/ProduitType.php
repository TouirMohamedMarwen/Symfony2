<?php

namespace TunisiaMole\BackOfficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProduitType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idcatalogue')
            ->add('idpromotion')
            ->add('libelle')
            ->add('reference')
            ->add('description')
            ->add('tva')
            ->add('prix')
            ->add('imageprodfront')
            ->add('imageprodback')
            ->add('imageprodreel')
            ->add('date')
            ->add('categorie')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TunisiaMole\BackOfficeBundle\Entity\Produit'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tunisiamole_backofficebundle_produit';
    }
}
