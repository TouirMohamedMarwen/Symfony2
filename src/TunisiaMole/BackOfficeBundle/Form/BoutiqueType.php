<?php

namespace TunisiaMole\BackOfficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BoutiqueType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelleboutique')
            ->add('categorieboutique')
            ->add('descriptionboutique')
            ->add('imageboutique')
            ->add('telephone')
            ->add('email')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TunisiaMole\BackOfficeBundle\Entity\Boutique'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tunisiamole_backofficebundle_boutique';
    }
}
