<?php

namespace TunisiaMole\BackOfficeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use TunisiaMole\BackOfficeBundle\Entity\Packpub;
use TunisiaMole\BackOfficeBundle\Form\PackpubType;

/**
 * Packpub controller.
 *
 */
class PackpubController extends Controller
{

    /**
     * Lists all Packpub entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackOfficeBundle:Packpub')->findAll();

        return $this->render('BackOfficeBundle:Packpub:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Packpub entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Packpub();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setProduitid($em->getRepository('BackOfficeBundle:Produit')->find($entity->getProduitid()));
            $em->persist($entity);
            
            
                    $entity = $em->getRepository('BackOfficeBundle:Packpub')->find($id);

            $em->flush();

            return $this->redirect($this->generateUrl('packpub_show', array('id' => $entity->getId())));
        }

        return $this->render('BackOfficeBundle:Packpub:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Packpub entity.
     *
     * @param Packpub $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Packpub $entity)
    {
        $form = $this->createForm(new PackpubType(), $entity, array(
            'action' => $this->generateUrl('packpub_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Packpub entity.
     *
     */
    public function newAction()
    {
        $entity = new Packpub();
        $form   = $this->createCreateForm($entity);

        return $this->render('BackOfficeBundle:Packpub:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Packpub entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackOfficeBundle:Packpub')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Packpub entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackOfficeBundle:Packpub:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Packpub entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackOfficeBundle:Packpub')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Packpub entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackOfficeBundle:Packpub:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Packpub entity.
    *
    * @param Packpub $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Packpub $entity)
    {
        $form = $this->createForm(new PackpubType(), $entity, array(
            'action' => $this->generateUrl('packpub_update', array('id' => $entity->getPackpub())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Packpub entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackOfficeBundle:Packpub')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Packpub entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('packpub_edit', array('id' => $id)));
        }

        return $this->render('BackOfficeBundle:Packpub:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Packpub entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackOfficeBundle:Packpub')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Packpub entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('packpub'));
    }

    /**
     * Creates a form to delete a Packpub entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('packpub_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
