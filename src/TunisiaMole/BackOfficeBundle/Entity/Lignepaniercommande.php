<?php

namespace TunisiaMole\BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lignepaniercommande
 *
 * @ORM\Table(name="lignepaniercommande", indexes={@ORM\Index(name="id_panier", columns={"idPanier"}), @ORM\Index(name="id_commande", columns={"idCommande"}), @ORM\Index(name="id_produit", columns={"idProduit"})})
 * @ORM\Entity
 */
class Lignepaniercommande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idLigne", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idligne;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPanier", type="integer", nullable=true)
     */
    private $idpanier;

    /**
     * @var string
     *
     * @ORM\Column(name="idCommande", type="string", length=255, nullable=true)
     */
    private $idcommande;

    /**
     * @var integer
     *
     * @ORM\Column(name="idProduit", type="integer", nullable=false)
     */
    private $idproduit;


}
