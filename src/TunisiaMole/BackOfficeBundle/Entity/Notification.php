<?php

namespace TunisiaMole\BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification", indexes={@ORM\Index(name="id_enseigne", columns={"idEnseigne"})})
 * @ORM\Entity
 */
class Notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idNotification", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idnotification;

    /**
     * @var integer
     *
     * @ORM\Column(name="idEnseigne", type="integer", nullable=false)
     */
    private $idenseigne;


}
