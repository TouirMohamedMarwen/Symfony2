<?php

namespace TunisiaMole\BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Catalogue
 *
 * @ORM\Table(name="catalogue", indexes={@ORM\Index(name="id_responsable_enseigne", columns={"id_responsable_enseigne"})})
 * @ORM\Entity
 */
class Catalogue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_catalogue", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCatalogue;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_responsable_enseigne", type="integer", nullable=false)
     */
    private $idResponsableEnseigne;


}
