<?php

namespace TunisiaMole\BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Recevoir
 *
 * @ORM\Table(name="recevoir", indexes={@ORM\Index(name="id_recevoir", columns={"idRecevoir", "idSuivre", "idNotification"}), @ORM\Index(name="id_suivre", columns={"idSuivre", "idNotification"}), @ORM\Index(name="id_notification", columns={"idNotification"})})
 * @ORM\Entity
 */
class Recevoir
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idRecevoir", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrecevoir;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSuivre", type="integer", nullable=false)
     */
    private $idsuivre;

    /**
     * @var integer
     *
     * @ORM\Column(name="idNotification", type="integer", nullable=false)
     */
    private $idnotification;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vu", type="boolean", nullable=false)
     */
    private $vu = '0';


}
