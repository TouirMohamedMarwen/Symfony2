<?php

namespace TunisiaMole\BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boutique
 *
 * @ORM\Table(name="boutique", indexes={@ORM\Index(name="id_enseigne", columns={"libelleBoutique"}), @ORM\Index(name="description_boutique", columns={"descriptionBoutique"})})
 * @ORM\Entity
 */
class Boutique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idBoutique", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idboutique;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleBoutique", type="string", length=15, nullable=false)
     */
    private $libelleboutique;

    /**
     * @var string
     *
     * @ORM\Column(name="categorieBoutique", type="string", length=15, nullable=false)
     */
    private $categorieboutique;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionBoutique", type="string", length=40, nullable=false)
     */
    private $descriptionboutique;

    /**
     * @var string
     *
     * @ORM\Column(name="imageBoutique", type="string", length=255, nullable=true)
     */
    private $imageboutique;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=11, nullable=false)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email;

    function getIdboutique() {
        return $this->idboutique;
    }

    function getLibelleboutique() {
        return $this->libelleboutique;
    }

    function getCategorieboutique() {
        return $this->categorieboutique;
    }

    function getDescriptionboutique() {
        return $this->descriptionboutique;
    }

    function getImageboutique() {
        return $this->imageboutique;
    }

    function getTelephone() {
        return $this->telephone;
    }

    function getEmail() {
        return $this->email;
    }

    function setIdboutique($idboutique) {
        $this->idboutique = $idboutique;
    }

    function setLibelleboutique($libelleboutique) {
        $this->libelleboutique = $libelleboutique;
    }

    function setCategorieboutique($categorieboutique) {
        $this->categorieboutique = $categorieboutique;
    }

    function setDescriptionboutique($descriptionboutique) {
        $this->descriptionboutique = $descriptionboutique;
    }

    function setImageboutique($imageboutique) {
        $this->imageboutique = $imageboutique;
    }

    function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    public function __toString()
{
    return (string) $this->getIdboutique();
}


}
