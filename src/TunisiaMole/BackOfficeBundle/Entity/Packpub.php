<?php

namespace TunisiaMole\BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Packpub
 *
 * @ORM\Table(name="packpub", indexes={@ORM\Index(name="ProduitId", columns={"ProduitId"}), @ORM\Index(name="BoutiqueId", columns={"BoutiqueId"})})
 * @ORM\Entity
 */
class Packpub
{
    /**
     * @var integer
     *
     * @ORM\Column(name="packPub", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $packpub;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \Produit
     *
     * @ORM\ManyToOne(targetEntity="Produit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ProduitId", referencedColumnName="idProduit")
     * })
     */
    private $produitid;

    /**
     * @var \Boutique
     *
     * @ORM\ManyToOne(targetEntity="Boutique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="BoutiqueId", referencedColumnName="idBoutique")
     * })
     */
    private $boutiqueid;

    function getPackpub() {
        return $this->packpub;
    }

    function getDescription() {
        return $this->description;
    }

    function getProduitid() {
        return $this->produitid;
    }

    function getBoutiqueid() {
        return $this->boutiqueid;
    }

    function setPackpub($packpub) {
        $this->packpub = $packpub;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setProduitid(\Produit $produitid) {
        $this->produitid = $produitid;
    }

    function setBoutiqueid(\Boutique $boutiqueid) {
        $this->boutiqueid = $boutiqueid;
    }


}
