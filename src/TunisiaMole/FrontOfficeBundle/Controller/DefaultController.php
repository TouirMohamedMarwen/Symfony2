<?php

namespace TunisiaMole\FrontOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        $produits = $em->getRepository('FrontOfficeBundle:Produit')->findAll();
        $categories=array();
        foreach ($produits as $value) {
            if (!in_array($value->getCategorie(),$categories)) {
               array_push($categories,$value->getCategorie());
            }
        }
        
        
        
        
        return $this->render('FrontOfficeBundle:Default:index.html.twig', array(
            'produits' => $produits,'categories' => $categories,
        ));
    }
}
