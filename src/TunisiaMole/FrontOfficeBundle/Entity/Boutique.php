<?php

namespace TunisiaMole\FrontOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boutique
 *
 * @ORM\Table(name="boutique", indexes={@ORM\Index(name="id_enseigne", columns={"libelleBoutique"}), @ORM\Index(name="description_boutique", columns={"descriptionBoutique"})})
 * @ORM\Entity
 */
class Boutique
{
    /**
     * @var string
     *
     * @ORM\Column(name="libelleBoutique", type="string", length=15, nullable=false)
     */
    private $libelleboutique;

    /**
     * @var string
     *
     * @ORM\Column(name="categorieBoutique", type="string", length=15, nullable=false)
     */
    private $categorieboutique;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionBoutique", type="string", length=40, nullable=false)
     */
    private $descriptionboutique;

    /**
     * @var string
     *
     * @ORM\Column(name="imageBoutique", type="string", length=255, nullable=true)
     */
    private $imageboutique;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=11, nullable=false)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="idBoutique", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idboutique;


}
