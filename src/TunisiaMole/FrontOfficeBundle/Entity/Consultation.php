<?php

namespace TunisiaMole\FrontOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Consultation
 *
 * @ORM\Table(name="consultation", indexes={@ORM\Index(name="id_client", columns={"idClient"}), @ORM\Index(name="id_enseigne", columns={"idEnseigne"})})
 * @ORM\Entity
 */
class Consultation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idClient", type="integer", nullable=false)
     */
    private $idclient;

    /**
     * @var integer
     *
     * @ORM\Column(name="idEnseigne", type="integer", nullable=false)
     */
    private $idenseigne;

    /**
     * @var integer
     *
     * @ORM\Column(name="idConsultation", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idconsultation;


}
