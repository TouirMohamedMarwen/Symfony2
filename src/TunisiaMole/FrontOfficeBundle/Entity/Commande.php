<?php

namespace TunisiaMole\FrontOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 *
 * @ORM\Table(name="commande", indexes={@ORM\Index(name="id_client", columns={"id_client"}), @ORM\Index(name="id_livraison", columns={"id_livraison"})})
 * @ORM\Entity
 */
class Commande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_client", type="integer", nullable=false)
     */
    private $idClient;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_livraison", type="integer", nullable=true)
     */
    private $idLivraison;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_commande", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCommande;

    function getIdClient() {
        return $this->idClient;
    }

    function getIdLivraison() {
        return $this->idLivraison;
    }

    function getIdCommande() {
        return $this->idCommande;
    }

    function setIdClient($idClient) {
        $this->idClient = $idClient;
    }

    function setIdLivraison($idLivraison) {
        $this->idLivraison = $idLivraison;
    }

    function setIdCommande($idCommande) {
        $this->idCommande = $idCommande;
    }


}
