<?php

namespace TunisiaMole\FrontOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity
 */
class Produit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idCatalogue", type="integer", nullable=true)
     */
    private $idcatalogue;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPromotion", type="integer", nullable=true)
     */
    private $idpromotion;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="tva", type="integer", nullable=true)
     */
    private $tva;

    /**
     * @var integer
     *
     * @ORM\Column(name="prix", type="integer", nullable=true)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="imageProdFront", type="string", length=255, nullable=true)
     */
    private $imageprodfront;

    /**
     * @var string
     *
     * @ORM\Column(name="imageProdBack", type="string", length=255, nullable=true)
     */
    private $imageprodback;

    /**
     * @var string
     *
     * @ORM\Column(name="imageProdReel", type="string", length=255, nullable=true)
     */
    private $imageprodreel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255, nullable=true)
     */
    private $categorie;

    /**
     * @var integer
     *
     * @ORM\Column(name="idProduit", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idproduit;
    function getIdcatalogue() {
        return $this->idcatalogue;
    }

    function getIdpromotion() {
        return $this->idpromotion;
    }

    function getLibelle() {
        return $this->libelle;
    }

    function getReference() {
        return $this->reference;
    }

    function getDescription() {
        return $this->description;
    }

    function getTva() {
        return $this->tva;
    }

    function getPrix() {
        return $this->prix;
    }

    function getImageprodfront() {
        return $this->imageprodfront;
    }

    function getImageprodback() {
        return $this->imageprodback;
    }

    function getImageprodreel() {
        return $this->imageprodreel;
    }

    function getDate() {
        return $this->date;
    }

    function getCategorie() {
        return $this->categorie;
    }

    function getIdproduit() {
        return $this->idproduit;
    }

    function setIdcatalogue($idcatalogue) {
        $this->idcatalogue = $idcatalogue;
    }

    function setIdpromotion($idpromotion) {
        $this->idpromotion = $idpromotion;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function setReference($reference) {
        $this->reference = $reference;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setTva($tva) {
        $this->tva = $tva;
    }

    function setPrix($prix) {
        $this->prix = $prix;
    }

    function setImageprodfront($imageprodfront) {
        $this->imageprodfront = $imageprodfront;
    }

    function setImageprodback($imageprodback) {
        $this->imageprodback = $imageprodback;
    }

    function setImageprodreel($imageprodreel) {
        $this->imageprodreel = $imageprodreel;
    }

    function setDate(\DateTime $date) {
        $this->date = $date;
    }

    function setCategorie($categorie) {
        $this->categorie = $categorie;
    }

    function setIdproduit($idproduit) {
        $this->idproduit = $idproduit;
    }



}
