<?php

namespace TunisiaMole\FrontOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Panier
 *
 * @ORM\Table(name="panier", indexes={@ORM\Index(name="id_client", columns={"idclient"}), @ORM\Index(name="f", columns={"idproduit"})})
 * @ORM\Entity
 */
class Panier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idclient", type="integer", nullable=true)
     */
    private $idclient;

    /**
     * @var integer
     *
     * @ORM\Column(name="idpanier", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpanier;

    /**
     * @var \TunisiaMole\FrontOfficeBundle\Entity\Produit
     *
     * @ORM\ManyToOne(targetEntity="TunisiaMole\FrontOfficeBundle\Entity\Produit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idproduit", referencedColumnName="idProduit")
     * })
     */
    private $idproduit;
  
}
