<?php

namespace TunisiaMole\FrontOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Suivre
 *
 * @ORM\Table(name="suivre", indexes={@ORM\Index(name="id_suivre", columns={"idSuivre", "idClient", "idEnseigne"}), @ORM\Index(name="id_client", columns={"idClient"}), @ORM\Index(name="id_enseigne", columns={"idEnseigne"})})
 * @ORM\Entity
 */
class Suivre
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idClient", type="integer", nullable=false)
     */
    private $idclient;

    /**
     * @var integer
     *
     * @ORM\Column(name="idEnseigne", type="integer", nullable=false)
     */
    private $idenseigne;

    /**
     * @var integer
     *
     * @ORM\Column(name="idSuivre", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idsuivre;


}
