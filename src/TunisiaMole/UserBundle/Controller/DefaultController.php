<?php

namespace TunisiaMole\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserBundle:Default:index.html.twig');
    }
    
      public function commandeAction(\Symfony\Component\HttpFoundation\Request $request)
    {   $data = $request->request->get('request');
   if ($data != NULL) {
       $entity = new \TunisiaMole\FrontOfficeBundle\Entity\Commande();
       $user=$this->getUser();
       
       $em = $this->getDoctrine()->getManager();
       $entity->setIdClient($user->getId());
       $em->persist($entity);
            $em->flush();
            
            return new \Symfony\Component\HttpFoundation\JsonResponse( array("name"=>$user->getUserName()));
   }
        
          return $this->render('UserBundle:Default:commande.html.twig');
    }
      
}
